﻿using TechTalk.SpecFlow;
using System;
using FluentAssertions;

namespace SpecFlowCalculator.Steps
{
    [Binding]
    public sealed class CalculatorSteps
    {

        // For additional details on SpecFlow step definitions see https://go.specflow.org/doc-stepdef

        private readonly ScenarioContext _scenarioContext;

        private readonly SpecFlowCalculator.Calculator _calculator = new SpecFlowCalculator.Calculator();
        

        public CalculatorSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        
        // a) example
        [Given(@"the number is (.*)")]
        public void GivenTheNumberIs(int number)
        {
            _calculator.Number = number;
        }
        private int _resultOCT;
        [When(@"the number is converted to OCT")]
        public void WhenTheNumberIsConvertedToOCT()
        {
            _resultOCT = _calculator.ConvertDecToOct();
        }

        [Then("the resultOCT should be (.*)")]
        public void ThenTheResultShouldBe(int result)
        {
            _resultOCT.Should().Be(result);
        }

        string resultHex;
        [When(@"the number is converted to HEX")]
        public void WhenTheNumberIsConvertedToHEX()
        {
            resultHex = _calculator.ConvertDecToHex();
        }

        [Then(@"the resultHEX should be (.*)")]
        public void ThenTheResultHEXShouldBeF(string result)
        {
            resultHex.Should().Be(result);
        }

        string resultBIN;
        [When(@"the number is converted to BIN")]
        public void WhenTheNumberIsConvertedToBIN()
        {
            resultBIN = _calculator.ConvertDecToBin();
        }

        [Then(@"the resultBIN should be (.*)")]
        public void ThenTheResultBINShouldBe(string result)
        {
            resultBIN.Should().Be(result);
        }
        //b example
        [Given(@"the first number is (.*)")]
        public void GivenTheFirstNumberIs(int number)
        {
            _calculator.FirstNumberOfAdding = number;
        }
        [Given(@"the second number is (.*)")]
        public void GivenTheSecondNumberIs(int number)
        {
            _calculator.SecondNumberOfAdding = number;
        }
        int _resultOfAdding = 0;
        [When(@"the two numbers are added")]
        public void WhenTheTwoNumbersAreAdded()
        {
            _resultOfAdding=_calculator.Add();
        }

        [Then(@"the resultOfAdding should be (.*)")]
        public void ThenTheResultOfAddingShouldBe(int result)
        {
            _resultOfAdding.Should().Be(result);
        }
        // c example
        [Given(@"the first date is (.*)")]
        public void GivenTheFirstDateIs(string p0)
        {
            DateTime Date1 = DateTime.ParseExact(p0, "dd/MM/yyyy", null);
            _calculator.FirstDate = Date1;
        }

        [Given(@"the second date is (.*)")]
        public void GivenTheSecondDateIs(string p0)
        {
            DateTime Date2 = DateTime.ParseExact(p0, "dd/MM/yyyy", null);
            _calculator.SecondDate = Date2;
        }

        string resultOfDates;
        [When(@"the two dates are subtracted")]
        public void WhenTheTwoDatesAreSubtracted()
        {
            resultOfDates = _calculator.SubtractDate();
        }

        
        [Then(@"the result should be (.*) days")]
        public void ThenTheResultShouldBeDays(string result)
        {
            resultOfDates.Should().Be(result);
        }










    }
}
