﻿Feature: Enter a number
	Change to OCT, HEX, BIN


@SmokeTag
Scenario: Change a value from DEC to OCT
	Given the number is 15
	When the number is converted to OCT
	Then the resultOCT should be 17


@SmokeTag2
Scenario: Change a value from DEC to HEX
	Given the number is 15
	When the number is converted to HEX
	Then the resultHEX should be F

@SmokeTag3
Scenario: Change a value from DEC to BIN
	Given the number is 15
	When the number is converted to BIN
	Then the resultBIN should be 1111