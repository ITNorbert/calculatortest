﻿Feature: CheckValue
	Simple calculator for adding two numbers

@Smoke4
Scenario: Add two numbers
	Given the first number is 120
	And the second number is 85
	When the two numbers are added
	Then the resultOfAdding should be 205