﻿Feature: CheckDate
	Differences between two dates

@Smoke5
Scenario: Subtracting two dates
	Given the first date is 15/02/2000
	And the second date is 31/05/2000
	When the two dates are subtracted
	Then the result should be 106 days