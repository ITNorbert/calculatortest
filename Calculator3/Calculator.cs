﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SpecFlowCalculator
{
    public class Calculator
    {
        public int Number { get; set; }
        public int FirstNumberOfAdding { get; set; }

        public int SecondNumberOfAdding { get; set; }

        public DateTime FirstDate { get; set; }

        public DateTime SecondDate { get; set; }

        public int Add()
        {
            //System.Diagnostics.Process.Start("calc");
            return FirstNumberOfAdding + SecondNumberOfAdding;
        }
        public int ConvertDecToOct()
        {
            List<int> octalNum = new List<int>();
            int i = 0;
            
            
                while (Number != 0)
                {

                    octalNum.Add(Number % 8);
                    Number = Number / 8;
                    i++;
                }
            int number =0;
            List<int> reverse = Enumerable.Reverse(octalNum).ToList();
            foreach (int entry in reverse)
            {
                number = 10 * number + entry;
            }
            return number;

        }

        public string ConvertDecToHex()
        {
            string hexValue = Number.ToString("X");
            return hexValue;
        }
        public string ConvertDecToBin()
        {
            string binaryValue = Convert.ToString(Number, 2);
            return binaryValue;

        }
        public string SubtractDate()
        {
            String diff = (SecondDate - FirstDate).TotalDays.ToString();
            return diff;

        }
    }
    
}